# Separator page

This page can be used as a separator. Simply open the page, enter a title (and optional Markdown description), and you've got an arbitrarily-named tab in your browser now.

## Screenshots

This screenshot shows the separator page being used to organize trees of tabs in Firefox. The separator page has the + (plus) icon on the left side. The tree-like structure is provided by the [Tree Style Tab extension](https://github.com/piroor/treestyletab/blob/trunk/README.md).

> ![Usage](usage.png)

This is the default view that comes up for a new separator page.

> ![Default View](default-view.png)

This is the edit-mode of the default view. Double-click on the title and then again on the contents to start editing them.

> ![Default Edit](default-edit.png)

## Installation

1. Navigate to [this page](https://pavelsafronov.gitlab.io/tools/separator-page/index.html)
2. Bookmark it
3. Edit the bookmark and remove the text after `index.html`
    - Or just replace the whole thing with `https://pavelsafronov.gitlab.io/tools/separator-page/index.html`
4. Start using the bookmark as a separator

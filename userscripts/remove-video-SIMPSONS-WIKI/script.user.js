// ==UserScript==
// @name         Simpsons Wiki Remove Video
// @namespace    https://gitlab.com/PavelSafronov/tools
// @version      0.1
// @description  Hide the video on Simpsons Wiki articles
// @author       Pavel Safronov
// @match        https://simpsons.fandom.com/wiki/*
// @icon         https://www.google.com/s2/favicons?domain=fandom.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const videos = document.querySelectorAll('[itemprop="video"]');
    if (videos) {
        const video = videos[0];
        if (video) {
            video.style.display = "none";
        }
    }
})();
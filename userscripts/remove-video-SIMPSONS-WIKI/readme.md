# Remove Video - Simpsons Wiki

This userscript removes the video element from the Simpsons Wiki.

## Installation

[Click here](https://gitlab.com/PavelSafronov/tools/-/raw/main/userscripts/remove-video-SIMPSONS-WIKI/script.user.js)

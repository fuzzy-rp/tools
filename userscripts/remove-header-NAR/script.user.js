// ==UserScript==
// @name         Remove NAR header
// @namespace    https://gitlab.com/PavelSafronov/tools
// @version      0.1
// @description  Remove NAR header
// @author       Pavel Safronov
// @match        https://notalwaysright.com/*
// @icon         https://www.google.com/s2/favicons?domain=notalwaysright.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const element = document.getElementById('site-header');
    element.remove();
})();
# Remove Header - Not Always Right

This userscript removes the header on [Not Always Right](https://notalwaysright.com).

## Installation

[Click here](https://gitlab.com/PavelSafronov/tools/-/raw/main/userscripts/remove-header-NAR/script.user.js)

// ==UserScript==
// @name         Click the red button
// @namespace    https://gitlab.com/PavelSafronov/tools
// @version      0.1
// @description  Click the red button on SMBC comics
// @author       Pavel Safronov
// @match        https://www.smbc-comics.com/*
// @icon         https://www.google.com/s2/favicons?domain=smbc-comics.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    toggleBlock("aftercomic");
})();
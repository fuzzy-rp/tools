# Click Red Button - SMBC script

This is a userscript that will click the red button on the comics website [SMBC](https://www.smbc-comics.com).

## Installation

[Click here](https://gitlab.com/PavelSafronov/tools/-/raw/main/userscripts/click-red-button-SMBC/script.user.js)

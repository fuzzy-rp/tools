# Mobile to Desktop - Wiki

This is a userscript that will redirect from mobile wiki pages (ewww!) to desktop wiki pages.

## Installation

[Click here](https://gitlab.com/PavelSafronov/tools/-/raw/main/userscripts/mobile-to-desktop-WIKI/script.user.js)

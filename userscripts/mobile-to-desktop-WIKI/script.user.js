// ==UserScript==
// @name         Redirect to non-mobile wiki page
// @namespace    https://gitlab.com/PavelSafronov/tools
// @version      0.1
// @description  Redirects mobile wiki pages (ewww) to non-mobile wiki pages
// @author       Pavel Safronov
// @match        https://en.m.wikipedia.org/wiki/*
// @icon         https://www.google.com/s2/favicons?domain=wikipedia.org
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    const newPath = window.location.href.replace(".m.wikipedia.org", ".wikipedia.org");
    window.location.href = newPath;
})();
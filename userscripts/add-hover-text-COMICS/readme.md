# Add Hover Text - Comics

This userscript adds the comics' hovertext to a div under the comic.

## Installation

[Click here](https://gitlab.com/PavelSafronov/tools/-/raw/main/userscripts/add-hover-text-COMICS/script.user.js)

// ==UserScript==
// @name         Add Hover Text to Webcomics
// @namespace    https://gitlab.com/PavelSafronov/tools
// @version      0.2
// @description  Add hover text as paragraph under comics.
// @author       Pavel Safronov
// @match        https://xkcd.com/*
// @match        https://www.smbc-comics.com/*
// @grant        none
// ==/UserScript==

(function () {
    'use strict';

    const hostnameToComicDivIdDict = {
        "www.smbc-comics.com": "cc-comicbody",
        "xkcd.com": "comic"
    }
    const hostname = window.location.hostname;
    const comicDivId = hostnameToComicDivIdDict[hostname];

    if (comicDivId) {
        const comicDiv = document.getElementById(comicDivId);
        const image = comicDiv.getElementsByTagName("img")[0];
        const altText = image && image.title;
        if (altText) {
            const p = document.createElement("p");
            p.innerText = `Hover text: ${altText}`;
            comicDiv.parentNode.insertBefore(p, comicDiv.nextSibling);
        }
    }
})();
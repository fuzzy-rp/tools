# tools

My personal tools: PowerShell profile scripts, TamperMonkey scripts, etc.

## Name

Pavel Safronov's commonly-used tools.

## Description

These are tools that Pavel Safronov wants to have available quickly and easily on any system he uses. The ultimate plan is to be able to run a single command and get all of these tools installed on a system.

### Minimum requirements

- [pwsh](https://github.com/PowerShell/PowerShell)
- [git](https://git-scm.com/downloads)
- [VS Code](https://code.visualstudio.com/download)
- [AutoHotkey](https://www.autohotkey.com/)
- [Tampermonkey](https://www.tampermonkey.net/) or similar extension for running userscripts

## Installation

TODO

## Userscripts

The follow userscripts are available. Install them from their readmes.

- [Add hover text to webcomics](https://gitlab.com/PavelSafronov/tools/-/tree/main/userscripts/add-hover-text-COMICS)
- [Automatically click red button on SMBC](https://gitlab.com/PavelSafronov/tools/-/tree/main/userscripts/click-red-button-SMBC/readme.md)
- [Remove header on Not Always Right](https://gitlab.com/PavelSafronov/tools/-/tree/main/userscripts/remove-header-NAR/readme.md)
- [Remove video div from Simpsons Wiki](https://gitlab.com/PavelSafronov/tools/-/tree/main/userscripts/remove-video-SIMPSONS-WIKI/readme.md)

## Separator page

A [page with a customizable title](https://gitlab.com/PavelSafronov/tools/-/tree/main/separator-page) that can be used as a separator.

## Support

If there are issues, open an issue in this repo.

## Roadmap

Ultimate goal is to be able to install most of these scripts with a single `pwsh` command.

## Contributing

Unsolicited PRs welcome.

## License

MIT

## Project status

Starting up.
